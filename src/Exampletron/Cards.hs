{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}

module Exampletron.Cards
    ( Suit, Card, Value, Deck, makeDeck
    ) where

import Data.List
import Data.Monoid
import Text.EscapeArtist

data Suit = Club | Spade | Heart | Diamond deriving (Eq, Ord, Bounded, Enum, Show)

data Value = Two | Three | Four | Five | Six | Seven | Eight | Nine | Ten | Jack | Queen | King | Ace deriving (Eq, Show, Enum, Ord, Bounded)

type Card = (Suit, Value)
type Deck = [Card]

makeDeck :: Deck
makeDeck = (,) <$> [Club ..] <*> [Two .. ]

instance ToEscapable Suit where
  toEscapable Heart   = FgRed $ show Heart
  toEscapable Diamond = FgRed $ show Diamond
  toEscapable Club    = FgBlue $ show Club
  toEscapable Spade   = FgBlue $ show Spade

instance ToEscapable Card where
  toEscapable (a, b) = Inherit "(" <> toEscapable a <> Inherit ", " <> (Inherit $ show b) <> Inherit ")"

instance ToEscapable Deck where
  toEscapable xs = mconcat $ intersperse (Inherit " ") $ map toEscapable xs
